# README #

## What is this repository for? ##

This repository contains some URL shortener scripts that fullfill the following specification:

### POST /shorten ###
* Parameters: Parameter link should contain the link to shorten.
* Returns: Id for the shortened link in text/plain format.
* POST requests use Content-Type: application/x-www-form-urlencoded unless otherwise mentioned

### GET /{id} ###
* Returns: 301 redirects the user agent to a previously stored URL. 404 error if no link stored with given id.

## Why multiple versions? ##

I first wanted to build the shortener with PHP, since I knew it would be quite simple. 
I thought building it as a standalone application would be considerably more difficult
(so I figured PHP wouldn't be "good enough") so I wanted to have a go at some other language.
The end result was that the python implementation didn't end up being any more complicated. 
They both ended up at around 100-110 lines.

## How do I get set up? ##

### Python ###
The python implementation stores the URLs in-memory.
Python version >= 3.3 is recommended. Won't run under Python 2.

Run the module normally with a Python 3 interpreter. The default port is 3000.
```
#!python
python ./python/url_shortener.py
```

The file **index.html** contains a test form for the Python script running at localhost:3000.

### PHP ###
The PHP code can be run under e.g. Apache. The code uses a MySQL database to store the URLs.
The **php/.htaccess** file turns the request URL into a GET parameter for the PHP script.
The file **php/index.php** filters out bad requests, responding with a HTTP 400 whenever there's a problem. 

The "php" directory contains everything needed, except the database info. 
You need to add a file called **php/db.php** which contains the following:
```
#!PHP
<?php
function getConnectionDetails() {
    return [
        'username' => 'your_username',
        'password' => 'your_password',
        'database' => 'your_database_name',
        'host' => 'your_mysql_host_name',
    ];
}
?>
```

You also need to enable the use of **.htaccess** files or copy the configuration to your server configuration files. 
The file **index.html** contains a test form for the PHP script.

Note: PHP version >= 5.4 is required.

## Who do I talk to? ##

* Email Teemu Vartiainen: etunimi.sukunimi [at] outlook.com
* Twitter: [@VartiainenTeemu](https://twitter.com/VartiainenTeemu)