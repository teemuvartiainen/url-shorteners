<?php
/*
 * Requires PHP >= 5.4
 */
if (version_compare(PHP_VERSION, '5.4.0', '<')) {
    header('HTTP/1.1 500 Internal Server Error');
    die('Your PHP version '.PHP_VERSION.' is not supported.');
}

// The file db.php contains a getConnectionDetails() function which returns an array.
// The array has the fields for a mysql database connection.
// This is so we are not committing database info into the repository. :)
include 'db.php';

// Functionality
class UrlShortener {
    
    const table_name = 'urlshortener_urls';
    private $db;
    
    function UrlShortener() {
        $params = getConnectionDetails();
        $this->db = new PDO("mysql:host={$params['host']};dbname={$params['database']}", $params['username'], $params['password'], [
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]);
    }
    
    private function createTables() {
        $this->db->query('CREATE TABLE `'.self::table_name.'` ('
                   .'`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY , '
                   .'`url` VARCHAR( 2083 ) NOT NULL '
                   .') ENGINE = INNODB;');
    }
    
    private function checkDatabase() {
        try {
            $result = $this->db->query('SELECT 1 FROM `'.self::table_name.'` LIMIT 1');
        } catch (PDOException $e) {
            $this->createTables();
        }
    }
    
    function shorten($valid_url) {
        $this->checkDatabase();
        $statement = $this->db->prepare('INSERT INTO `'.self::table_name.'` (`url`) VALUES (?);');
        $statement->execute([$valid_url]);
        $newId = $this->db->lastInsertId();
        header('Content-Type: text/plain');
        echo $newId;
    }
    
    function redirect($valid_id) {
        $this->checkDatabase();
        $statement = $this->db->prepare('SELECT `url` FROM `'.self::table_name.'` WHERE `id`=? LIMIT 1;');
        $statement->execute([$valid_id]);
        if ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            http_response_code(301);
            header('Location: '.$row['url']);
        } else {
            // 404 Not Found
            http_response_code(404);
            echo "Link identifier not found from the database.";
        }
    }
    
}

// Request handling conditionals
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (isset($_GET['action']) && is_numeric($_GET['action'])) {
        try {
            $urlId = $_GET['action'];
            $shortener = new UrlShortener();
            $shortener->redirect($urlId);
        } catch (PDOException $e) {
            // 500 Internal server error
            http_response_code(500);
            echo "Database error when retrieving data.";
        }
    } else {
        // 400 Bad request
        http_response_code(400);
        echo "Invalid request action, numeric link identifier expected.";
    }
} elseif ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_GET['action']) && $_GET['action'] === 'shorten') {
    if ($_SERVER['CONTENT_TYPE'] === 'application/x-www-form-urlencoded') {
        // Make sure that the parameter is a valid url
        if (isset($_POST['link']) || !filter_var($_POST['link'], FILTER_VALIDATE_URL)) {
            try {
                $link = $_POST['link'];
                $shortener = new UrlShortener();
                $shortener->shorten($link);
            } catch (PDOException $e) {
                // 500 Internal server error
                http_response_code(500);
                echo "Database error.";
            }
        } else {
            // 400 Bad request
            http_response_code(400);
            echo "Invalid request, link parameter missing.";
        }
    } else {
        // 400 Bad request
        http_response_code(400);
        echo "Invalid request content type, expected application/x-www-form-urlencoded.";
    }
} else {
    // 400 Bad request
    http_response_code(400);
    echo "Invalid request.";
}

?>