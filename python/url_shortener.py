from http.server import BaseHTTPRequestHandler,HTTPServer
from urllib.parse import parse_qs
import sqlite3

local_port = 3000

class url_shortener(BaseHTTPRequestHandler):

    # in-memory database (also known as a list)
    url_database = []

    # Called by the HTTPServer when a POST request is received
    def do_POST(self):
        if (self.is_valid_post_request()):
            link = self.formdata_get_valid_link();
            if (link):
                identifier = self.save_url(link)
                self.output_identifier(identifier)
            else:
                self.send_error(400)
        else:
            self.send_error(400)

    # Called by the HTTPServer when a GET request is received
    def do_GET(self):
        id = self.is_valid_get_request()
        if (id >= 0):
            if (self.is_valid_identifier(id)):
                self.send_redirection(self.url_database[id])
            else:
                self.send_error(404)
        else:
            self.send_error(400)

    #--------------------------------------------------#
    #                   POST helpers                   #
    #--------------------------------------------------#

    def is_valid_post_request(self):
        if (self.path != '/shorten'):
            print('Invalid path')
            return False
        if (self.headers['Content-Type'] != 'application/x-www-form-urlencoded'):
            print('Invalid content type')
            return False
        if (not self.headers['Content-Length']):
            print('Invalid content length')
            return False
        return True

    def formdata_get_valid_link(self):
        content_len = int(self.headers['Content-Length'])
        linkdata = self.rfile.read(content_len)
        form = parse_qs(linkdata.decode('utf-8'))
        if (form and form['link'] and form['link'][0]):
            return form['link'][0]
        else:
            return False

    def save_url(self, url):
        identifier = len(self.url_database)
        self.url_database.append(url)
        return "%d" % identifier

    def output_identifier(self, identifier):
        self.send_response(200)
        self.send_header('Content-type','text/plain')
        self.end_headers()
        self.wfile.write(identifier.encode('utf-8'))

    #--------------------------------------------------#
    #                   GET helpers                    #
    #--------------------------------------------------#

    def is_valid_get_request(self):
        try: 
            identifier = int(self.path[1:])
        except:
            return -1
        return identifier

    def is_valid_identifier(self, id):
        if (id >= 0 and id < len(self.url_database)):
            return True
        else:
            return False

    def send_redirection(self, url):
        self.send_response(301)
        self.send_header('Location',url)
        self.end_headers()

# Run the actual server
try:
    print('Starting server on port',local_port)
    server = HTTPServer(('', local_port), url_shortener)
    server.serve_forever()
except KeyboardInterrupt:
    print('Closing server...')
    server.socket.close()


